import Vue from 'vue'
import UserTransformer from './../../transformers/user'

// When the request succeeds
const success = (response) => {
  if (!response) {
    // eslint-disable-next-line
    console.debug("")
  }
  Vue.router.push({
    name: 'login.index',
  })
}

// When the request fails
const failed = () => {
}

export default (user) => {
  Vue.$http.post('/account/signup/', UserTransformer.send(user))
      .then((response) => {
        success(response)
      }).catch((error) => {
        failed(error)
      })
}
